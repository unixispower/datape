# DATAPE Codec
DATAPE is a data storage format that is intended to be reliable when used on
aging audio cassette tape hardware. The `datape` utility is used to encode or
decode a byte stream to a DATAPE audio stream.

This program is VERY unfinished. This README will include instructions on
installation and usage upon initial release. For more information check out the
[DATAPE](https://hackaday.io/project/27917-datape) project on
[hackaday.io](https://hackaday.io/).
