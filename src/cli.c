/*
 * Command line interface functionality.
 */

#include "cli.h"
#include "program.h"

#include <getopt.h>

#include <limits.h>
#include <errno.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>


/*
 * Parse a `rate` option argument.
 *
 * This function parses an validates a rate. The range of rates is less than
 * the full range of `unsigned long` because `strtol()` is used to allow
 * detection of negative numbers. This function reports errors using
 * `cli_print_error()` for convenience.
 *
 * short_opt: short option used in error reporting
 * opt_arg: string containing a <rate>
 * rate: out argument
 * return: error_success on success
 * return: error_invalid_args on failure
 */
error cli_parse_rate(char short_opt, const char *opt_arg, unsigned long *rate) {
    char *end;
    errno = 0; // reset error
    long raw_rate = strtol(opt_arg, &end, 10);

    // check if raw value is valid
    if (*end != '\0' || errno || raw_rate < 1) {
        cli_print_error(
            error_invalid_args,
            "Value of \"-%c\" must be in [1,%ld].", short_opt, LONG_MAX);
        return error_invalid_args;
    }

    *rate = raw_rate;
    return error_success;
}

/* Print usage string to specified stream. */
void cli_print_usage(FILE *stream) {
    const char *usage =
        "Usage:\n"
        "  " PROGRAM_NAME " -e [-i <file>] [-o <file>] [-s <rate>] [-b <rate>]\n"
        "  " PROGRAM_NAME " -d [-i <file>] [-o <file>]\n"
        PROGRAM_DESC "\n"
        "\n"
        "Modes:\n"
        "  -e --encode  Encode bytes from stdin and write audio samples to stdout\n"
        "  -d --decode  Decode audio samples from stdin and write bytes to stdout\n"
        "\n"
        "Options:\n"
        "  -i --input=<file>        Read input from a file instead of stdin\n"
        "  -o --output=<file>       Write output to a file instead of stdout\n"
        "  -s --sample-rate=<rate>  Sample rate of audio output, default 48000\n"
        "  -b --baud-rate=<rate>    Baud rate of data in audio output, default 4800\n"
        "  -h --help                Print this help message then exit\n"
        "  -v --version             Print the program version then exit\n"
        "\n"
        "If a file is specified as \"-\", then a stdio stream will be used.\n"
        "Audio is consumed and produced as a headerless stream of 16 bit signed PCM\n"
        "samples that match platform endianess.\n";
    fprintf(stream, usage);
}

/* Print version string to specified stream. */
void cli_print_version(FILE *stream) {
    fprintf(stream, PROGRAM_TITLE " " PROGRAM_VERSION "\n");
    fprintf(stream, "Copyright (c) " PROGRAM_COPYRIGHT "\n");
    fprintf(stream, "Released under " PROGRAM_LICENSE " license\n");
}

/* Parse command line arguments. */
error cli_parse_args(cli_args *args, int argc, char *argv[]) {
    const char *short_opts = ":hvedi:o:s:b:";
    const struct option long_opts[] = {
        // meta modes
        {"help",    no_argument, NULL, 'h'},
        {"version", no_argument, NULL, 'v'},

        // codec modes
        {"encode",  no_argument, NULL, 'e'},
        {"decode",  no_argument, NULL, 'd'},

        // common options 
        {"input",  required_argument, NULL, 'i'},
        {"output", required_argument, NULL, 'o'},

        // encode options
        {"sample-rate", required_argument, NULL, 's'},
        {"baud-rate",   required_argument, NULL, 'b'},

        {0, 0, 0, 0}
    };
    bool mode_set = false;

    opterr = 0; // disable getopt printing

    // reset arguments to defaults
    args->input       = "-";
    args->output      = "-";
    args->sample_rate = CLI_DEFAULT_SAMPLE_RATE;
    args->baud_rate   = CLI_DEFAULT_BAUD_RATE;

    // process option flags
    error err;
    while (1) {
        char short_opt = getopt_long(argc, argv, short_opts, long_opts, NULL);
        if (short_opt == -1) {
            break;
        }

        switch (short_opt) {
            // help
            case 'h':
                args->mode = cli_mode_help;
                return error_success;

            // version
            case 'v':
                args->mode = cli_mode_version;
                return error_success;

            // encode/decode
            case 'e':
            case 'd':
                // prevent multiple mode options
                if (mode_set) {
                    cli_print_error(
                        error_invalid_args,
                        "Modes \"-e\" and \"-d\" cannot be combined.");
                    return error_invalid_args;
                }
                args->mode = (short_opt == 'e')
                    ? cli_mode_encode : cli_mode_decode;
                mode_set = true;
                break;

            // input
            case 'i':
                args->input = optarg;
                break;

            // output
            case 'o':
                args->output = optarg;
                break;

            // sample rate
            case 's':
                err = cli_parse_rate(short_opt, optarg, &(args->sample_rate));
                if (err) {
                    return err;
                }
                break;

            // baud rate
            case 'b':
                err = cli_parse_rate(short_opt, optarg, &(args->baud_rate));
                if (err) {
                    return err;
                }
                break;

            // missing value
            case ':':
                cli_print_error(
                    error_invalid_args,
                    "Option \"-%c\" requires a value.", optopt);
                return error_invalid_args;

            // unknown option
            case '?':
                if (optopt) {
                    // short option
                    cli_print_error(
                        error_invalid_args,
                        "Unknown option \"-%c\".", optopt);
                } else {
                    // long option
                    cli_print_error(
                        error_invalid_args,
                        "Unknown option \"%s\".", argv[optind - 1]);
                }
                return error_invalid_args;
        }
    }

    // require mode to be set
    if (!mode_set) {
        cli_print_error(
            error_invalid_args, "Must specify mode \"-e\" or \"-d\".");
        return error_invalid_args;
    }

    // limit baud rate to sample rate
    if (args->baud_rate > args->sample_rate) {
        cli_print_error(
            error_invalid_args,
            "Value of \"-b\" must not be greater than value of \"-s\".");
        return error_invalid_args;
    }

    // disallow non-option arguments
    int path_count = argc - optind;
    if (path_count > 0) {
        cli_print_error(
            error_invalid_args,
            "Unknown option \"%s\".", argv[optind]);
        return error_invalid_args;
    }

    return error_success;
}

/* Print message of `err` and contextual information to stderr. */
void cli_print_error(error err, const char *info, ...) {
    fputs(error_message(err), stderr);
    fputc(' ', stderr);

    va_list args;
    va_start(args, info);
    vfprintf(stderr, info, args);
    va_end(args);

    fputc('\n', stderr);
}

/* Print external error message and contextual information to stderr. */
void cli_print_error_str(const char *message, const char *info, ...) {
    // ensure message and info are separated with a period
    fputs(message, stderr);
    size_t length = strlen(message);
    if (length > 0 && message[length - 1] != '.') {
        fputc('.', stderr);
    }
    fputc(' ', stderr);

    va_list args;
    va_start(args, info);
    vfprintf(stderr, info, args);
    va_end(args);

    fputc('\n', stderr);
}

/* Indicate if a path represents a stdio stream. */
bool cli_is_stdio(const char *path) {
    return strcmp(path, "-") == 0;
}
