/*
 * DATAPE Codec mainfile.
 *
 * This module contains main() and high-level program functionality.
 */

#include "cli.h"
#include "error.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/* Open a file or standard stream for use as a byte stream. */
error open(FILE **file, const char *path, const char* mode) {
    if (cli_is_stdio(path)) {
        *file = strcmp(mode, "r") == 0 ? stdin : stdout;
        return error_success;
    }
    *file = fopen(path, mode);
    if (errno) {
        cli_print_error_str(
            strerror(errno), "Could not open file \"%s\".", path);
        return error_file_access;
    }
    return error_success;
}

/* Close a `FILE` opened with `open()`. */
error close(FILE *file) {
    if (file == NULL) {
        return error_success;
    }
    if (file != stdin && file != stdout) {
        int err = fclose(file);
        if (err == EOF) {
            cli_print_error_str(strerror(errno), "Could not close file.");
            return error_file_access;
        }
    }
    return error_success;
}

/* Encode bytes from `data_file` and write audio samples to `audio_file`. */
error encode(FILE *data_file, FILE *audio_file) {
    printf("TESTING (encode)\n");
    return error_success;
}

/* Decode audio samples from `audio_file` and write bytes to `data_file`. */
error decode(FILE *audio_file, FILE *data_file) {
    printf("TESTING (decode)\n");
    return error_success;
}

/* Open files, call `encode()` or `decode()`, and cleanup.  */
error encodecode(cli_args *args) {
    error err = error_success;
    FILE *data_file = NULL;
    FILE *audio_file = NULL;

    // encode
    if (args->mode == cli_mode_encode) {
        err = open(&data_file, args->input, "r");
        if (err) {
            goto cleanup;
        }
        err = open(&audio_file, args->output, "w");
        if (err) {
            goto cleanup;
        }
        err = encode(data_file, audio_file);
    // decode
    }  else /* if (args->mode == cli_mode_decode) */ {
        err = open(&audio_file, args->input, "r");
        if (err) {
            goto cleanup;
        }
        err = open(&data_file, args->output, "w");
        if (err) {
            goto cleanup;
        }
        err = decode(audio_file, data_file);
    }

    // clean up resources
    error audio_close_err, data_close_err;
cleanup:
    audio_close_err = close(audio_file);
    data_close_err = close(data_file);

    if (err) {
        return err;
    }
    if (audio_close_err || data_close_err) {
        return error_file_access;
    }
    return error_success;
}

int main(int argc, char *argv[]) {
    cli_args args;
    error err;

    err = cli_parse_args(&args, argc, argv);
    if (err) {
        cli_print_usage(stderr);
        return EXIT_FAILURE;
    }

    switch (args.mode) {
        case cli_mode_help:
            cli_print_usage(stdout);
            break;

        case cli_mode_version:
            cli_print_version(stdout);
            break;

        case cli_mode_encode:
        case cli_mode_decode:
            err = encodecode(&args);
            if (err) {
                return EXIT_FAILURE;
            }
            break;
    }

    return EXIT_SUCCESS;
}
