/*
 * Command line interface functionality.
 *
 * This module contains usage message, error message, and command line argument
 * parsing functionality.
 */

#ifndef CLI_H
#define CLI_H

#include "error.h"

#include <stdbool.h>
#include <stdio.h>

#define CLI_DEFAULT_SAMPLE_RATE (48000)
#define CLI_DEFAULT_BAUD_RATE   (4800)


/*
 * Mode of program execution.
 */
typedef enum cli_mode {
    cli_mode_help,    // show help screen
    cli_mode_version, // show version number
    cli_mode_encode,  // convert data to audio
    cli_mode_decode   // convert audio to data
} cli_mode;

/*
 * Structure used to store parsed command line arguments.
 */
typedef struct cli_args {
    cli_mode mode;
    char *input;
    char *output;
    unsigned long sample_rate;
    unsigned long baud_rate;
} cli_args;

/*
 * Print usage string to specified stream.
 */
void cli_print_usage(FILE *stream);

/*
 * Print version string to specified stream.
 */
void cli_print_version(FILE *stream);

/*
 * Parse command line arguments.
 * To simplify error handling this function reports specific errors using
 * `cli_print_error()` and returns the value `error_invalid_args` on failure.
 *
 * args: container for parsed arguments
 * argv: argv from main()
 * argc: argc from main()
 * return: error_success if arguments are paresed successfully
 * return: error_invalid_args if arguments cannot be parsed
 */
error cli_parse_args(cli_args *args, int argc, char *argv[]);

/*
 * Print message of `err` and contextual information to stderr.
 * The `info` argument is treated as a printf() format string for the
 * remaining arguments.
 *
 * err: error to print message of
 * info: information message format string
 * ...: placeholder values for `info` string
 */
void cli_print_error(error err, const char *info, ...);

/*
 * Print external error message and contextual information to stderr.
 * The `info` argument is treated as a printf() format string for the
 * remaining arguments.
 *
 * message: error message
 * info: information message format string
 * ...: placeholder values for `info` string
 */
void cli_print_error_str(const char *message, const char *info, ...);

/*
 * Indicate if a path represents a stdio stream.
 *
 * path: file path to check
 * return: true if path is "-", false otherwise
 */
bool cli_is_stdio(const char *path);

#endif /* CLI_H */
