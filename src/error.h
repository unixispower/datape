/*
 * Error codes and messages.
 *
 * This module contains an error enumeration and functionality to retrieve a
 * human-readable message for an error.
 */

#ifndef ERROR_H
#define ERROR_H


/*
 * Error codes returned by various functions.
 */
typedef enum error {
    error_success = 0,
    error_invalid_args, // invalid command line usage
    error_file_access,  // error accessing a file
} error;

/*
 * Retrieve the message for an error.
 *
 * err: error to retrieve message for
 * return: pointer to error message
 */
const char * error_message(error err);


#endif /* ERROR_H */
