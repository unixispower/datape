/*
 * Error codes and messages.
 */

#include "error.h"

/* Retrieve the message for an error. */
const char * error_message(error err) {
    switch (err) {
        case error_invalid_args:
            return "Invalid command arguments.";
        case error_file_access:
            return "Could not access file.";

        // unknown/generic error
        default:
            return "Unknown error.";
    }
}
